#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect.hpp>
#include <iostream>
#include <memory>

#include "quirc.h"

using namespace cv;

quirc_decode_error_t binaryDecode(Mat in, std::string &resultString, struct error_correction_info &ecInfo) {


    // create and empty quirc_code struct
    quirc_code qr_code;
    memset(&qr_code, 0, sizeof(qr_code));
    qr_code.size = 29;

    // fill the bitmap 
    for (int y = 0; y < qr_code.size; ++y) {
        auto row = in.ptr<uchar>(y);
        for (int x = 0; x < qr_code.size; ++x) {
            int position = y * qr_code.size + x;
            qr_code.cell_bitmap[position >> 3]
                |= row[x] ? 0 : (1 << (position & 7));
        }
    }

    quirc_data qr_code_data;

    // call the modified decoding function from Quirc
    quirc_decode_error_t errorCode = quirc_decode(&qr_code, &qr_code_data, &ecInfo);
    if (errorCode != 0) { 
        return errorCode; 
    }

    // concatenate the payload data into single string
    resultString = "";
    for (int i = 0; i < qr_code_data.payload_len; i++) {
        resultString += qr_code_data.payload[i];
    }
    return errorCode;
}

int main(int argc, char **argv)
{
    Mat image;
    image = imread(argv[1], IMREAD_GRAYSCALE); // Read the file

    if (!image.data) // Check for invalid image file
    {
        std::cout << "Could not open or find the image" << std::endl;
        return -1;
    }

    struct error_correction_info ecInfo;
    std::string message;
    quirc_decode_error_t errorCode = binaryDecode(image, message, ecInfo);
    if (errorCode == 0) {
        std::cout << "QR code message: " << message << std::endl;
        std::cout << "Number of information codewords: " << ecInfo.info_codewords_count << std::endl;
        std::cout << "Number of corrected information codewords: " << ecInfo.corrected_info_codewords_count << std::endl;
        std::cout << "Number of corrected bits in format1: " << ecInfo.format1_error_count << std::endl;
        std::cout << "Number of corrected bits in format2: " << ecInfo.format2_error_count << std::endl;
        for (int i = 0; i < ecInfo.corrected_bits_count; ++i) {
            std::cout << "Corrected bit: " << "x = " << ecInfo.corrected_bits[i] % 29 << " y = " << ecInfo.corrected_bits[i] / 29 << std::endl;
        }
    }
    else {
        std::cout << "QR code decoding failed" << std::endl;
    }
    return 0;
}